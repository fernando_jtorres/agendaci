<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard_model extends CI_model
	{
		public function index($id)
		{
			$this->db->order_by("nome", "ASC");
			$this->db->where("Usuario_idUsuario", $id);
			return $this->db->get("contato", 5)->result_array();
		}

		public function userlist()
		{
			$this->db->order_by("idUsuario");
			return $this->db->get("usuario")->result_array();
		}

		public function contactlist($id)
		{
			$this->db->order_by("idContato");
			$this->db->where("Usuario_idUsuario", $id);
			return $this->db->get("contato")->result_array();
		}

		public function storecontact($contato)
		{
			$this->db->insert('contato', $contato);
			return $this->db->affected_rows();
		}
		
		public function getcontato($idcontato, $idusuario)
		{
			$this->db->where('Usuario_idUsuario', $idusuario);
			return $this->db->get_where('contato', array('idContato' => $idcontato))
			->row_array();
		}

		public function update($idcontato, $contato)
		{
			$this->db->where("idContato", $idcontato);
			return $this->db->update("contato", $contato);
		}

		public function delete($idcontato)
		{
			$this->db->where("idContato", $idcontato);
			return $this->db->delete('contato'); // retorna true se a operação for realizada
		}
		
		public function search($nomepesquisado, $idusuario)
		{
			$this->db->where('Usuario_idUsuario', $idusuario);
			$this->db->like('nome', $nomepesquisado);
			return $this->db->get("contato")->result_array();
		}

	}

?>