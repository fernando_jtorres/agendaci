<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Inicio_model extends CI_model
	{
		public function register($user)
		{
			$this->db->insert('usuario', $user);
			return $this->db->affected_rows();
		}

		public function login($email, $senha)
		{
			$this->db->where("email", $email);
			$this->db->where("senha", $senha);
			$user = $this->db->get("usuario")->row_array();
			return $user;
		}

		public function checkemail($email){
			$this->db->where("email", $email);
			return $this->db->get('usuario')->row_array();
		}
		
	}

?>