<?php 
  $this->load->view('dashboardparts/header');
  $this->load->view('dashboardparts/navbar');
?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Edição de Contato</h4>
          <!-- <p class="card-category">Complete your profile</p> -->
        </div>
        <div class="card-body">
          <form method="post" action="<?= base_url('') ?>dashboard/update/<?= $contato['idContato'] ?>">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="bmd-label-floating">Nome</label>
                  <input value="<?= $contato['nome'] ?>" name="nome"  type="text" class="form-control" required>
                </div>
              </div>
            </div>
            <div class="row mt-2">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="bmd-label-floating">Telefone</label>
                  <input value="<?= $contato['telefone'] ?>" name="telefone" type="int" class="form-control"required>
                </div>
              </div>
            </div>
            <div class="row mt-2">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="bmd-label-floating">Email</label>
                  <input value="<?= $contato['email'] ?>" name="email" type="email" class="form-control"required>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary pull-left mt-3">Salvar alterações</button>
            <div class="clearfix"></div>
          </form>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

<?php 
  $this->load->view('dashboardparts/footer');
?>