<?php 
  $this->load->view('dashboardparts/header');
  $this->load->view('dashboardparts/navbar');
?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title"><?= $tabletitle ?></h4>
            <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
          </div>
          <div class="card-body table-responsive">
            
            <!-- Verificar se tem contato cadastrado -->
            <?php if( count($contatos) != 0 ){  ?>
            <table class="table table-hover">
              <thead class="text-warning">
                <th>Id Contato</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Email</th>
                <th>Ações</th>
              </thead>
              <tbody>
                <?php foreach($contatos as $contatos) : ?>
                <tr>
                  <td><?= $contatos["idContato"] ?></td>
                  <td><?= $contatos["nome"] ?></td>
                  <td><?= $contatos["telefone"] ?></td>
                  <td><?= $contatos["email"] ?></td>
                  <td> 
                    <a href="<?= base_url() ?>dashboard/edit/<?= $contatos["idContato"] ?>" type="button" class="btn btn-warning btn-sm">
                    Editar &nbsp;<i class="material-icons">edit</i>
                    </a>

                    <a href="javascript:goDelete(<?= $contatos['idContato'] ?>)" type="button" class="btn btn-danger btn-sm">
                      Deletar &nbsp;<i class="material-icons">delete</i>
                    </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php }else{  ?>
              <p class="text-center h4">Nenhum resultado encontrado!</p>
            <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
	function goDelete(id) {
		if(confirm("Deseja apagar este contato?")) {
			window.location.href = 'delete/'+id;
		}
	}
</script>

<?php 
  $this->load->view('dashboardparts/footer');
?>