<?php 
  $this->load->view('dashboardparts/header');
  $this->load->view('dashboardparts/navbar');
?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Lista de Usuários</h4>
            <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
          </div>
          <div class="card-body table-responsive">
            <!-- Verificar se tem contato cadastrado -->
            <?php if( count($usuarios) != 0 ){  ?>
            <table class="table table-hover">
              <thead class="text-warning">
                <th>Id Usuário</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Bio</th>
              </thead>
              <tbody>
                <?php foreach($usuarios as $usuarios) : ?>
                <tr>
                  <td><?= $usuarios["idUsuario"] ?></td>
                  <td><?= $usuarios["nome"] ?></td>
                  <td><?= $usuarios["email"] ?></td>
                  <td><?= $usuarios["bio"] ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php }else{  ?>
              <p class="text-center h4">A lista de usuários está vazia.</p>
            <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php 
  $this->load->view('dashboardparts/footer');
?>