<?php 
  $this->load->view('dashboardparts/header');
  $this->load->view('dashboardparts/navbar');
?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-8 col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Últimos 5 contatos adicionados</h4>
            <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
          </div>
          <div class="card-body table-responsive">
            <!-- Verificar se tem contato cadastrado -->
            <?php if( count($contatos) != 0 ){  ?>
            <table class="table table-hover">
              <thead class="text-warning">
                <th>Id Contato</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Email</th>
              </thead>
              <tbody>
                <?php foreach($contatos as $contatos) : ?>
                <tr>
                  <td><?= $contatos["idContato"] ?></td>
                  <td><?= $contatos["nome"] ?></td>
                  <td><?= $contatos["telefone"] ?></td>
                  <td><?= $contatos["email"] ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php }else{  ?>
              <p class="text-center h4">A lista de contatos está vazia.</p>
            <?php } ?>

          </div>
        </div>
        
        <div class="card mt-5">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Informações do usuário</h4>
            <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
                <th>Id Usuário</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Bio</th>
              </thead>
              <tbody>
                <tr>
                  <td><?= $this->session->logged_user['idUsuario'] ?></td>
                  <td><?= $this->session->logged_user['nome'] ?></td>
                  <td><?= $this->session->logged_user['email'] ?></td>
                  <td><?= $this->session->logged_user['bio'] ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <div class="card">
          <div class="card-header card-header-danger">
            <h4 class="card-title">Serviços da Agenda</h4>
            <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
                <th></th>
                <th>Funcionalidade</th>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Cadastrar Contato</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Listar Contato</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Atualizar contato</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Deletar Contatos</td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Pesquisar Contatos</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
    </div>
    </div>
  </div>
</div>

<?php 
  $this->load->view('dashboardparts/footer');
?>