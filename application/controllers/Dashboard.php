<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		permission();
		$this->load->model("dashboard_model");
	}

	public function index()
	{
		$id = $this->session->logged_user['idUsuario'];
		$data['title'] = 'Dashboard';
		$data['contatos'] = $this->dashboard_model->index($id);
		# função active na barra lateral
		$data['d'] = 'active';
		$data['cc'] = $data['lc'] = $data['lu'] = '';
		$this->load->view('dashboard', $data);
	}

	public function userlist()
	{
		$data['title'] = 'Lista de Usuários';
		$data['usuarios'] = $this->dashboard_model->userlist();
		# função active na barra lateral
		$data['lu'] = 'active';
		$data['cc'] = $data['lc'] = $data['d'] = '';
		$this->load->view('userlist', $data);
	}
	
	public function contactlist()
	{
		$id = $this->session->logged_user['idUsuario'];
		$data['title'] = 'Lista de Contatos';
		$data['contatos'] = $this->dashboard_model->contactlist($id);
		# função active na barra lateral
		$data['lc'] = 'active';
		$data['cc'] = $data['d'] = $data['lu'] = '';
		$this->load->view('contactlist', $data);
	}
	
	public function formcontact()
	{
		$data['title'] = 'Cadastrar Contato';
		# função active na barra lateral
		$data['cc'] = 'active';
		$data['d'] = $data['lc'] = $data['lu'] = '';
		$this->load->view('formcontact', $data);
	}

	public function storecontact()
	{
		$contato = $_POST;
		$contato['Usuario_idUsuario'] = $this->session->logged_user['idUsuario'];
		$result = $this->dashboard_model->storecontact($contato);
		# função active na barra lateral
		$data['cc'] = 'active';
		$data['d'] = $data['lc'] = $data['lu'] = '';

		if ($result == 1) {
			$data['title'] = 'Cadastrar Contato';
			$data['resultado'] = 'success';
			$data['texto'] = 'Contato cadastrado com sucesso!';
			$this->load->view('formcontact', $data);
		}else{
			$data['title'] = 'Cadastrar Contato';
			$data['resultado'] = 'danger';
			$data['texto'] = 'Falha ao cadastrar!';
			$this->load->view('formcontact', $data);
		}
	}
	
	public function edit($id)
	{
		$data['title'] = 'Editar Contato';
		$data['contato'] = $this->dashboard_model->getcontato($id, $this->session->logged_user['idUsuario'] );
		# função active na barra lateral
		$data['lc'] = 'active';
		$data['d'] = $data['cc'] = $data['lu'] = '';
		$this->load->view('editcontact', $data);
	}

	public function update($idcontato)
	{
		$contato = $_POST;
		$result = $this->dashboard_model->update($idcontato, $contato);
		redirect('dashboard/contactlist');
	}

	public function delete($idcontato)
	{
		$this->dashboard_model->delete($idcontato);
		redirect('dashboard/contactlist');
	}

	public function search()
	{
		if (!isset($_POST['nomepesquisado'])) { #verifica se o post tem informações
			$_POST['nomepesquisado'] = '';
			redirect('dashboard');
		}
		$data['title'] = 'Resultado da pesquisa';
		$data['tabletitle'] = 'Contatos relacionados a * <b>'. $_POST['nomepesquisado'] . ' </b> *';
		$data['contatos'] = $this->dashboard_model->search($_POST['nomepesquisado'], $this->session->logged_user['idUsuario']);
		# função active na barra lateral
		$data['d'] = 'active';
		$data['cc'] = $data['lc'] = $data['lu'] = '';
		$this->load->view('searchresult', $data);
	}

	public function logout()
	{
		$this->session->unset_userdata("logged_user");
		redirect('inicio');
	}

}
