<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("inicio_model");
	}

	public function index()
	{
		$this->load->view('landingpage');
	}

	public function register()
	{
		// Verificar se já existe email
		$answer = $this->inicio_model->checkemail($_POST["email"]);
		if (isset($answer)) {
			$_POST['msgregister'] = array(
				'tipo' => 'danger',
				'texto' => 'E-mail já cadastrado');
		}else {
			$user = array(
				"nome" => $_POST["name"],
				"email" => $_POST["email"],
				"senha" => md5($_POST["password"]),
				"bio" => $_POST["bio"],
				"tipo" => '0'
			);
				
			$result = $this->inicio_model->register($user);

			if ( isset($result) ) {
				self::login();
			}else {
				$_POST['msgregister'] = array(
					'tipo' => 'danger',
					'texto' => 'Erro no cadastro');
			}
		}#fim primeiro else
		$this->load->view('landingpage');
	}
	public function login()
	{
		$email = $_POST["email"];
		$senha = md5($_POST["password"]);
		$user = $this->inicio_model->login($email, $senha);
		if($user){
			$this->session->set_userdata("logged_user", $user);
			redirect("dashboard");
			//$this->session->logged_user['nome'];
		}else{
			$_POST['msglogin'] = array(
				'tipo' => 'danger',
				'texto' => 'Usuário não encontrado!' );
			$this->load->view('landingpage');
		}
	}

}
