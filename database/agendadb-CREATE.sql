-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema agendadb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema agendadb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `agendadb` DEFAULT CHARACTER SET utf8 ;
USE `agendadb` ;

-- -----------------------------------------------------
-- Table `agendadb`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agendadb`.`usuario` (
  `idUsuario` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `email` VARCHAR(250) NOT NULL,
  `senha` VARCHAR(250) NOT NULL,
  `bio` TEXT NULL DEFAULT NULL,
  `tipo` INT(11) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `agendadb`.`contato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agendadb`.`contato` (
  `idContato` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `telefone` VARCHAR(20) NOT NULL,
  `email` VARCHAR(250) NULL DEFAULT NULL,
  `Usuario_idUsuario` BIGINT(20) NOT NULL,
  PRIMARY KEY (`idContato`),
  INDEX `fk_Contato_Usuario_idx` (`Usuario_idUsuario` ASC) VISIBLE,
  CONSTRAINT `fk_Contato_Usuario`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `agendadb`.`usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
